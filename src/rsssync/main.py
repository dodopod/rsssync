import logging

from argparse import ArgumentParser
from pathlib import Path

import feedparser
import toml

from .downloader import downloaders


def main():
    """Downloads RSS updates based on command line arguments."""

    # Parse args
    parser = ArgumentParser(description="Download RSS updates")

    parser.add_argument('-n', '--num-entries', type=int, default=0,
        metavar='N', help="Download last N updates")
    parser.add_argument('-s', '--subfolders', action='store_true',
        help="Store each feed's updates in its own subfolder")
    parser.add_argument('-d', '--downloader', action='append',
        dest='downloaders', help="Download handler")
    parser.add_argument('-m', '--mkpath', action='store_true',
        help="Create destination path, if it doesn't exist")
    parser.add_argument('-v', '--verbose', action='count', default=0,
        help="Print messages to console")
    parser.add_argument('-r', '--redownload', action='store_true',
        help="Ignore cache and redownload all entries")
    parser.add_argument('srcs', nargs='+', metavar='src',
        help="URL of an RSS or Atom feed")
    parser.add_argument('dest', type=Path,
        help="Directory feeds are downloaded to")

    args = parser.parse_args()

    if not args.downloaders:    # default downloader
        args.downloaders = ['meta']

    # Setup logger
    logging.basicConfig(
        level = logging.WARNING - 10 * args.verbose,
        format = "%(message)s")

    logger = logging.getLogger(__name__)

    logger.info(f"Updating folder \"{args.dest}\"")

    # Mkpath option
    if args.mkpath:
        args.dest.mkdir(parents=True, exist_ok=True)

    # Open/create cache
    cache_path = args.dest.joinpath('.cache').with_suffix('.toml')

    if cache_path.exists() and not args.redownload:
        cache = toml.load(cache_path)
    else:
        cache = {}

    # Set user-agent string
    feedparser.USER_AGENT = "RSSsync/0.2"

    # Download feeds
    for src in args.srcs:
        for downloader_name in args.downloaders:
            cache_section = f"{downloader_name}:{src}"
            cache.setdefault(cache_section, {})
            downloader = downloaders[downloader_name](
                src, args.dest,
                cache[cache_section]
            )

            downloader.download_feed()

            if downloader.is_feed_modified:
                if args.subfolders:
                    downloader.create_subfolder()

                downloader.download_entries(args.num_entries)

    # Save cache
    with cache_path.open('w') as f:
        toml.dump(cache, f)
